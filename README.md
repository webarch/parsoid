Ansible Playbooks to configure a Debian Stretch VM to be a 
[Parsoid server](https://www.mediawiki.org/wiki/Parsoid/Setup#Ubuntu_.2F_Debian), 
see also the [MediaWiki Playbooks](https://git.coop/webarch/mediawiki).

```bash
export DISTJRO="stretch"
export SERVERNAME="parsoid.webarch.net"
ansible-playbook parsoid.yml -u root -i ${SERVERNAME}, -e "hostname=${SERVERNAME}"
```
